/*Created on : 16/10/2019, 19:50:50    Author     : haguilerts*/
Array.mail();

/***************************** TEORIA Métodos de un Arreglo en Javascript: ******************************/
/*  https://developer.mozilla.org/es/docs/Web/JavaScript/Referencia/Objetos_globales/Array/forEach
 * 
Array.from()
Array.isArray()
Array.observe()
Array.of()
Array.prototype.concat()
Array.prototype.copyWithin()
Array.prototype.entries()
Array.prototype.every()
Array.prototype.fill()
Array.prototype.filter()
Array.prototype.find()
Array.prototype.findIndex()
Array.prototype.flat()
Array.prototype.flatMap()
Array.prototype.forEach()
Array.prototype.includes()
Array.prototype.indexOf()
Array.prototype.join()
Array.prototype.keys()
Array.prototype.lastIndexOf()
Array.prototype.map()
Array.prototype.pop()
Array.prototype.push()
Array.prototype.reduce()
Array.prototype.reduceRight()
Array.prototype.reverse()
Array.prototype.shift()
Array.prototype.slice()
Array.prototype.some()
Array.prototype.sort()
Array.prototype.splice()
Array.prototype.toLocaleString()
Array.prototype.toSource()
Array.prototype.toString()
Array.prototype.unshift()
Array.prototype.values()
Array.prototype[@@iterator]()
Array.unobserve()
get Array[@@species]
 * 
    En Javascript existen muchos métodos para arreglos muy importantes al momento de desarrollar lógica en alguna de nuestras aplicaciones, 
a continuación explico de forma breve el uso de Filter, ForEach, Map y Find.
Estos métodos poseen en común que reciben una función "callback" que permite 3 atributos, los cuales son: ELEMENTOS, INDICE y ARREGLO. 
Por lo general utilizamos el atributo ELEMENTO pero es bueno que conozcamos que podemos utilizar los otros atributos para algunas funciones mas específicas.

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    -----------Filter-----------

    El método Filter crea un nuevo arreglo, el cual no busca modificar el valor de los elementos para el nuevo arreglo, 
únicamente retorna aquellos elementos que cumplan con las condiciones del filtro, los cuales serán almacenados en otro arreglo.
    
    let array = [
        { id: 1, username: "Mapper"},
        { id: 2, username: "Finder"},
        { id: 3, username: "Eaching"}
    ];
    let filtered = array.filter(function(element){
      return element.id > 2;
    }); 
        console.log(filtered);
    //Output: [{ id: "3", username: "Eaching"}]

                /////////// otra forma de escribir  /////////

    let filterId = array.filter(myFuntion);
        function myFuntion(element){
                return element.id === 3;
        }
        console.log(filterId);
        //Output: [{ id: "3", username: "Eaching"}]                    

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    -----------MAP-----------

    El método Map tambien se encarga de crear un "nuevo arreglo" sin cambiar el anterior, al igual que el metodo Filter. 
Cada elemento es multiplicado por 1000 y a su vez se almacenan en un nuevo arreglo llamado miles.
    
    let array = [1, 2, 3, 4];
    let miles = array.map(function(element) {
      return element * 1000;
    });
    //Output: [1000, 2000, 3000, 4000]

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    -----------FIND-----------

    Con la ayuda del método Find podemos encontrar el "Primer Valor" que cumpla con función que definimos,
en el siguiente ejemplo podemos observar que el arreglo posee 5 elementos numéricos,
posteriormente en la variable "valor" ejecutamos el método find y retornamos el elemento que sea mayor a 100 
obteniendo como resultado el primer valor del arreglo que cumple con la condicion.

    let array = [14, 123, 50, 20, 312];

    let valor = array.find(function(element) {
    return element > 100;
    });
    //Output: 123
                                
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////  
    -----------ForEach-----------
  
    El método ForEach únicamente ejecuta la función callback (vulve a llamar de vuelta) que definimos, en el siguiente ejemplo
únicamente muestra como salida cada elemento del arreglo. Cabe destacar, 
que no retorna ningún valor ni tampoco un arreglo como los métodos anteriores.
  
    let array = [10, 20, 30, 40, 50];
    array.forEach(function(element) {
    console.log(element);
    });
    //Output: 10, 20, 30, 40, 50

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    -----------Every-----------
    El método every() verifica si "todos" los valores del vector "pasan en un determinado test" Y obtendrías un true o false.
    El método every() devuelve un booleano, true si "todos" los elementos en el arreglo pasan la condición 
implementada por la función dada y false si alguno no la cumple
   
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    -----------Some-----------
    Contrario de every(), el método some() verifica si "alguno" de los valores del vector "pasa un determinado test".
    El método some() comprueba si al menos "un" elemento del array cumple con la condición implementada por la función proporcionada

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////    
    -----------Reduce-----------
    El método reduce() ejecuta una función en cada uno de los elementos del vector para “reducirlo” a un valor único. 
Es importante tener en cuenta que reduce() funciona de izquierda a derecha y no “reduce” el vector original.
    
    let array = [
        { username: "Mapper", apellido: "Hernandes", edad=20 },
        { username: "Finder", apellido: "bargas",  edad=10 },
        { username: "Eaching", apellido: "aguilar",  edad=40 }
    ];
    let sum = array.filter(getUser).map(getEdad).reduce(getEdTotal)
        console.log(sum);

    funtion.getUser(id){
      return id.username === "Finder";  //toma el primer array     
    }
    funtion.getEdad(id){
      return id.edad * 2; // toma un valor del array y opera
    }
    funtion.getEdTotal(total, id){
      return total + id ; // retorna en objeto
    }
    
    );
    //Output: [{ id: "3", username: "Eaching"}]

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////    
    -----------forEach-----------
    forEach() llama y ejecuta una función una vez por cada elemento del vector. Comparando con map() o reduce(), 
forEach() es ideal cuando lo que quieres no es cambiar los datos de tu vector, si no guardarlos en alguna base de datos. 
forEach() siempre devuelve el valor undefined y no es encadenable. Lo podrías ejecutar de la siguiente forma:

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    -----------Every-----------

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    -----------Every-----------

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    -----------Every-----------


    

  */
