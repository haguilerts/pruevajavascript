/*Created on : 16/10/2019, 19:50:30    Author     : haguilerts*/
class Array {
     static BBDD(){
        let cuentas = [
            {usuario: "gio",    password1: "1",     edad: 11  },
            {usuario: "gio",    password1: "12",    edad: 21 },
            {usuario: "gio",    password1: "123",   edad: 31 },
            {usuario: "marco",  password: "123m",   edad: 42 },
            {usuario: "sergio", password: "123s",   edad: 52 },
            {usuario: "natu",   password: "123n",   edad: 62 },
            {usuario: "efi",    password: "123e",   edad: 10 }
            ];
        return cuentas;
    }
    
//    static Map(){
//        console.log("provando metodo map ");
//
//        //------------  array tradicional---------------
//        let  datoCuenta = Array.BBDD();// lo traigo de la BBDD en formato array
//        let usuarioCuentaGuardado= []; // añado una nueva variable vacio para usuarios. 
//        let passCuentaGuardado= []; // añado una nueva variable vacio para paswords.
//            // recorro toda la longitud con el for imprimiedo solo los usuarios.
//        for(let i=0; i<datoCuenta.length; i++){
//            usuarioCuentaGuardado.push(datoCuenta[i].usuario);// añado todo los "usuarios:" de "datoCuenta" al "datoCuentaGuardado" del tipo array que estaba vacio.
//            passCuentaGuardado.push(datoCuenta[i].password);// añado todo los "Pasword:" de "datoCuenta" al "datoCuentaGuardado" del tipo array que estaba vacio.
//            
//        }
//                console.log("Usuarios:");
//            console.log(usuarioCuentaGuardado); // muestra los usuarios del array.
//                console.log("Password:");
//            console.log(passCuentaGuardado); // muestra los Pasword del array.
//        // datoCuenta.map(usuarioPorUsuario => console.log());
//
//        //------------ otra forma de hacer el array (usando metodo)---------------
//        let  datoCuenta = Array.BBDD();// lo trae todo, usuario y password de la BBDD en formato array
//        let miDatoUsuarioArry = datoCuenta.map(function (datoUsuario) {
//            //console.log(dato.password);
//            return datoUsuario.usuario; // retorna la contraseña, este metodo funciona como un GET y SET..
//        });
//        let miDatoPassArry = datoCuenta.map(function (datoPass) {
//            //console.log(dato.password);
//            return datoPass.password; // retorna la contraseña, este metodo funciona como un GET y SET..
//        });
//        console.log("Usuarios:");
//        console.log(miDatoUsuarioArry);
//        console.log("password:");
//        console.log(miDatoPassArry);
//    }
//    
//    static Filter(){
//        console.log("provando metodo filter...");
//        //------------  array tradicional---------------
//        let  datoCuenta = Array.BBDD();// lo trae todo, usuario y password de la BBDD en formato array
//        let passCoincidentes= []; // añado una nueva variable vacio para usuarios. 
//        for(let i=0; i<datoCuenta.length; i++){
//            // hago una comparacion si datoCuenta que viene de BBD = a "gio" entonces q muestre las contraseñas disponible
//            if (datoCuenta[i].usuario === "gio") {
//                console.log("bienvenido: "+ datoCuenta[i].usuario);
//                passCoincidentes.push(datoCuenta[i].password);// añado todo los "password:" de "datoCuenta" al "datoCuentaGuardado" del tipo array que estaba vacio.
//                
//            } else {
//            } 
//        } console.log(passCoincidentes); //muestras las contraseñas disponibles
//        
//        //------------ 2° forma de hacer el array (usando metodo)---------------
//        let  datoCuenta = Array.BBDD();// lo trae todo, usuario y password de la BBDD en formato array
//        // tomo los datos atraves de filtes y comparo los usuarios y tomo los q coinciden. con el map solo le pido q me retorne la contraseña
//        let passCoincidentes = datoCuenta
//            .filter(function (usu) {
//                return usu.usuario === "gio";
//            })
//            .find(function (usu){
//                return usu.password;
//            });            
//            console.log("password de Gio:");
//            console.log(passCoincidentes);
//            
//        //------------ 3° forma de hacer el array para edad(usando metodo)---------------   
//        let edad = datoCuenta.filter(myFuntion);
//        function myFuntion(element){
//                return element.edad=== 3;
//        }
//           console.log("edad de Gio:");
//            console.log(edad); 
//    }
// 
//    static Find(){
//        console.log("provando metodo Find...");
//        
//        // ************* 1 forma *************        
//        let  datoCuenta = Array.BBDD();// lo trae todo, usuario y password de la BBDD en formato array
//        // tomo los datos atraves de filtes y comparo los usuarios y terno los q coinciden. con el map solo le pido q me retorne la contraseña
//        let usuCoincidentes = datoCuenta.find( usu => 
//                console.log(usu.usuario)==="gio"                   
//            );
//            console.log("usuario ::");
//            console.log(usuCoincidentes);
//            
//        // ************* 2 forma *************     
//        let usuCoincidentes2 = datoCuenta.find( usu => {
//            return (/gio/g).test(usu.usuario);
//        });
//            console.log("usuario2 ::");
//            console.log(usuCoincidentes2);
//   
//    }
//    
//    static Some(){
//            let  datoCuenta = Array.BBDD();
//            let check1= datoCuenta.some( item =>{
//                return item.usuario==="marco" ; // si es verdadero devuelve true, si es falso devuelve false
//            } );
//            console.log("SomeUsu ::");
//            console.log(check1);
//           let check2= datoCuenta.filter(todo).some(pass);
//                       
//            function todo(id){
//                return id.usuario==="marco";
//            }            
//            function pass(id){
//                return id.password==="123m" ; 
//            }
//            console.log("SomePass ::");
//            console.log(check2); 
//    }
//    
//    static Every(){ // devuelve true o false
//            console.log("--estas en emtodo Everi---");
//
//        let  datoCuenta = Array.BBDD();
//        let checkEvery= datoCuenta.filter(usu).every(ed);
//                function usu (id) {
//                  return id.usuario === "gio"; // ingreso a array q cumplan esa condicion 
//                }
//                function ed (id) {
//                  return id.edad >0; // solo dara true si y solo si toda la condion se cumpla
//                }                   
//        console.log("todosEdad>0 ?? ::"+checkEvery);            
//                      
//    ////////////////////////////// 
//        var array1 = [1, 30, 39, 29, 10, 13];
//        let total =array1.every(id);
//        function id (currentValue) {
//            return currentValue = 13;
//        }
//        console.log("alguno=13 ??::"+total);
//        // expected output: true
//        
//    } 
//    
//    static ForEach(){
//       let  datoCuenta = Array.BBDD();
//       
//       let a = datoCuenta.forEach(obj);
//        function obj(id) {            
//          console.log(id); // trare todo lo q esta en el array
//        } 
//        // console.log(a); no puedo hacerlo 
//    }    
//       
//    static Reduce() {
//            console.log("estas en metodo reduce");
//            let  datoCuenta = Array.BBDD();
//
//            let array = [
//            { username: "Mapper", apellido: "Hernandes", edad: 22 },
//            { username: "Finder", apellido: "bargas",  edad: 15 },
//            { username: "Eaching", apellido: "aguilar",  edad: 10 }
//            ];
//        let sum = datoCuenta.filter(getUser).map(getEdad).reduce(getEdTotal);
//
//            function getUser(id){
//                return id.usuario === "sergio";
//                // selecciona el vector q cumple con la condicion 
//            }
//            function getEdad(id){
//                return id.edad * 2;
//                // toma la edad y lo multiplica por 2 y devuelve en array
//            } 
//            function getEdTotal(total, id){
//                return total + id ;
//                // recuce lo comverte ese valor en objeto
//            } 
//            console.log(sum);
//            //Output: 104
//    }  
//    
//    static Push() {
//            console.log("provando metodo push...");
//
//            // El método push() añade uno o más elementos al final de un array y devuelve la nueva longitud del array
//            let animales = ["gato", "perro", "rata"]; // armo mi array
//
//            let total = animales.push("mono"); // agrego mono al array de animales
//            console.log(total);  // imprime la cantidad = 4
//            console.log(animales); // imprime el incluido: Array ["gato", "perro", "rata", "mono"]
//
//            animales.push("Tigre", "Leon", "Gorila"); //agrego Tigre, leon y gorila al array
//            console.log(animales);  // imprime el Array completo incluidos: Array ["gato", "perro", "rata", "mono", Tigre", "Leon", "Gorila"]
//    }
//    
    static prueva(){
        console.log("estamos en prueva..");
        let myBBDD=Array.BBDD();
        
        let a =myBBDD.filter(usu);
            function usu(id){
                return  id.usuario==="natu";
            }
             console.log("edad: "+a[0].edad);
//        function sal(id){
//            return console.log( id.edad);
//        }
//        let b = myBBDD.find( id  => {
//                    If(id.usuario === "natu"){
//                     return console.log(id.saldo);
//                     
//                    }
//                   });
           
//             let usuCoincidentes2 = datoCuenta.find( usu => {
//            return (/gio/g).test(usu.usuario);
//        });
        //console.log(myBBDD);
        console.log(a);
//        console.log(b);
    }
    
    static mail(){ 
        console.log("estamos en mail..");
        //console.log("estas en mail.prueva");
        //Array.BBDD();
        
        //Array.Map();
        //Array.Filter();
        //Array.Find();
        //Array.Some();
        //Array.Every();
        //Array.ForEach();        
        //Array.Reduce();
        //Array.Push();
       this.prueva();
    }
}




